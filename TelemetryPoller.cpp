#include "TelemetryPoller.h"

#include "SharedMemory.h"
#include <windows.h>
#include <string>
#include <iostream>
#include <sstream>

using namespace std;

// Name of the pCars memory mapped file
const char* const FILE_MAPPING_OBJECT_NAME = "$pcars2$";

namespace
{
    class FileMapping
    {
    public:
        FileMapping(const string& fileMappingObjectName)
        {
            m_handle = OpenFileMapping(PAGE_READONLY, FALSE, fileMappingObjectName.c_str());
            if (m_handle == NULL)
            {
                ostringstream stream;
                stream << "Could not open file mapping object - Error code:" << GetLastError() << endl;
                throw runtime_error(stream.str());
            }
        }

        ~FileMapping()
        {
            CloseHandle(m_handle);
        }

        const HANDLE& GetHandle()
        {
            return m_handle;
        }

    private:
        // This handle is guaranteed to be not NULL after successful construction
        HANDLE m_handle;
    };

    class FileMappingView
    {
    public:
        FileMappingView(const HANDLE& fileMappingObjectHandle)
        {
            m_view = (Telemetry*)MapViewOfFile(fileMappingObjectHandle, PAGE_READONLY, 0, 0, sizeof(Telemetry));
            if (m_view == nullptr)
            {
                ostringstream stream;
                stream << "Could not map view of file - Error code: " << GetLastError() << endl;
                throw runtime_error{ stream.str() };
            }

            if (m_view->mVersion != SHARED_MEMORY_VERSION)
            {
                ostringstream stream;
                stream << "Data version mismatch. Expected: " << SHARED_MEMORY_VERSION
                    << ". Found: " << m_view->mVersion << endl;
                throw runtime_error{ stream.str() };
            }
        }

        ~FileMappingView()
        {
            UnmapViewOfFile(m_view);
        }

        const Telemetry& GetView()
        {
            return *m_view;
        }

    private:
        // This pointer is guaranteed to be non-null after successful construction
        const Telemetry* m_view;
    };

    struct TelemetryPollerResources
    {
        TelemetryPollerResources()
            : fileMapping{ nullptr }
            , fileMappingView{ nullptr }
            , lastSequenceNumber{ 0 }
        {
        }

        unique_ptr<int> i;
        unique_ptr<FileMapping> fileMapping;
        unique_ptr<FileMappingView> fileMappingView;
        unsigned int lastSequenceNumber;
    };
}

TelemetryPoller::TelemetryPoller()
    : m_resources{ make_unique<TelemetryPollerResources>() }
{
    m_resources->fileMapping = make_unique<FileMapping>(FILE_MAPPING_OBJECT_NAME);
    m_resources->fileMappingView = make_unique<FileMappingView>(m_resources->fileMapping->GetHandle());
}

TelemetryPoller::~TelemetryPoller() = default;

unique_ptr<Telemetry> TelemetryPoller::Next()
{
    const Telemetry& sharedMemory = m_resources->fileMappingView->GetView();

    auto sequenceNumber = sharedMemory.mSequenceNumber;

    // If the sequence number hasn't changed, the data isn't new
    if (sequenceNumber == m_resources->lastSequenceNumber)
        return nullptr;

    // Sequence number is incremented to an odd number on start of write, and incremented to an even number at end of
    // write. If the sequence number is odd, a write is in progress
    if (sequenceNumber % 2 != 0)
        return nullptr;

    unique_ptr<Telemetry> localCopy = make_unique<Telemetry>();
    memcpy(localCopy.get(), &sharedMemory, sizeof(Telemetry));

    {
        auto sequenceNumberBeforeCopy = sequenceNumber;
        auto sequenceNumberAfterCopy = localCopy->mSequenceNumber;

        // Check if the memory was written while we were copying it
        if (sequenceNumberBeforeCopy != sequenceNumberAfterCopy)
            return nullptr;
    }

    m_resources->lastSequenceNumber = localCopy->mSequenceNumber;
    return localCopy;
}