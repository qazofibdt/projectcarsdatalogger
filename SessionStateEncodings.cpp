#include "SessionStateEncodings.h"

#include "SharedMemory.h"

#include <unordered_map>

using namespace std;

namespace
{
    const unordered_map<int, string> lookup{
        { SESSION_INVALID,      "Invalid" },
        { SESSION_PRACTICE,     "Practice" },
        { SESSION_TEST,         "Test" },
        { SESSION_QUALIFY,      "Qualifying" },
        { SESSION_FORMATION_LAP, "Formation Lap" },
        { SESSION_RACE,         "Race" },
        { SESSION_TIME_ATTACK,  "Time Attack" },
    };
}

string EncodeSessionState(int val)
{
    return lookup.at(val);
}
