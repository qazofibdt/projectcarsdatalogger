#include "CSVFile.h"

#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;

namespace
{
    class OFileStream
    {
    public:
        OFileStream(const string& filepath)
            : m_stream{ make_unique<ofstream>(filepath, ofstream::out | ofstream::trunc) }
        {
            if (!m_stream->is_open())
            {
                ostringstream stream;
                stream << "Unable to open file: " << filepath << endl;
                throw runtime_error{ stream.str() };
            }
        }

        ofstream& GetStream()
        {
            return *m_stream;
        }

        ~OFileStream()
        {
            m_stream->close();
        }

    private:
        // This is guaranteed to be a non-null pointer to an open file stream after successful construction
        unique_ptr<ofstream> m_stream;
    };

    struct CSVFileResources
    {
        CSVFileResources()
            : csvFileStream{ nullptr }
            , rowLength{ 0 }
            , delimiter{ 0 }
        {
        }

        unique_ptr<OFileStream> csvFileStream;
        unsigned int rowLength;
        char delimiter;
    };

    template<typename It>
    unsigned int WriteCSVRow(ostream& stream, It rowBegin, It rowEnd, char delimiter=',')
    {
        unsigned int rowLength = 0;

        if (rowBegin != rowEnd)
        {
            stream << *rowBegin;
            rowLength++;
        }

        for (auto it = rowBegin + 1; it != rowEnd; ++it)
        {
            stream << delimiter;
            stream << *it;
            rowLength++;
        }

        stream << endl;

        return rowLength;
    }
}

CSVFile::CSVFile(const string& filepath, const vector<string>& headers, char delimiter)
    : m_resources{ make_unique<CSVFileResources>() }
{
    m_resources->delimiter = delimiter;
    m_resources->csvFileStream = make_unique<OFileStream>(filepath);

    auto& stream = m_resources->csvFileStream->GetStream();
    m_resources->rowLength = WriteCSVRow(stream, begin(headers), end(headers));
}

CSVFile::~CSVFile() = default;

void CSVFile::WriteRow(const vector<string>& tokens)
{
    auto& stream = m_resources->csvFileStream->GetStream();
    unsigned int rowLength = WriteCSVRow(stream, begin(tokens), end(tokens), m_resources->delimiter);

    if (rowLength != m_resources->rowLength)
    {
        cout << "WARNING: Inconsistent CSV row length. Expected " << m_resources->rowLength
            << ", found " << rowLength << endl;
    }
}
