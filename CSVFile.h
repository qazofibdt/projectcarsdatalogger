#pragma once

#include <memory>
#include <vector>

using namespace std;

namespace
{
    struct CSVFileResources;
}

class CSVFile
{
public:
    CSVFile(const string& filepath, const vector<string>& headers, char delimiter=',');
    ~CSVFile();

    void WriteRow(const vector<string>& tokens);

private:
    unique_ptr<::CSVFileResources> m_resources;
};

