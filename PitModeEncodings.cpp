#include "PitModeEncodings.h"

#include "SharedMemory.h"

#include <unordered_map>

using namespace std;

namespace
{
    const unordered_map<int, string> lookup{
        { PIT_MODE_NONE,                    "None" },
        { PIT_MODE_DRIVING_INTO_PITS,       "Driving Into Pits" },
        { PIT_MODE_IN_PIT,                  "In Pit" },
        { PIT_MODE_DRIVING_OUT_OF_PITS,     "Driving Out of Pits" },
        { PIT_MODE_IN_GARAGE,               "In Garage" },
        { PIT_MODE_DRIVING_OUT_OF_GARAGE,   "Driving Out of Garage" },
    };
}

string EncodePitMode(int val)
{
    return lookup.at(val);
}
