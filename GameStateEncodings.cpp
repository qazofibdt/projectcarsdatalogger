#include "GameStateEncodings.h"

#include "SharedMemory.h"

#include <unordered_map>

using namespace std;

namespace
{
    const unordered_map<int, string> lookup{
        { GAME_EXITED,              "Exited" },
        { GAME_FRONT_END,           "Front End" },
        { GAME_INGAME_PLAYING,      "InGame Playing" },
        { GAME_INGAME_PAUSED,       "InGame Paused" },
        { GAME_INGAME_INMENU_TIME_TICKING, "InGame InMenu Time Ticking" },
        { GAME_INGAME_RESTARTING,   "InGame Restarting" },
        { GAME_INGAME_REPLAY,       "InGame Replay" },
        { GAME_FRONT_END_REPLAY,    "Front End Replay" },
    };
}

string EncodeGameState(int val)
{
    return lookup.at(val);
}
