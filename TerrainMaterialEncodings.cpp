#include "TerrainMaterialEncodings.h"

#include "SharedMemory.h"

#include <unordered_map>

using namespace std;

namespace
{
    const unordered_map<int, string> lookup{
        { TERRAIN_ROAD,             "Road" },
        { TERRAIN_LOW_GRIP_ROAD,    "Low Grip Road" },
        { TERRAIN_BUMPY_ROAD1,      "Bump Road 1" },
        { TERRAIN_BUMPY_ROAD2,      "Bump Road 2" },
        { TERRAIN_BUMPY_ROAD3,      "Bump Road 3" },
        { TERRAIN_MARBLES,          "Marbles" },
        { TERRAIN_GRASSY_BERMS,     "Grassy Berms" },
        { TERRAIN_GRASS,            "Grass" },
        { TERRAIN_GRAVEL,           "Gravel" },
        { TERRAIN_BUMPY_GRAVEL,     "Bumpy Gravel" },
        { TERRAIN_RUMBLE_STRIPS,    "Rumble Strips" },
        { TERRAIN_DRAINS,           "Drains" },
        { TERRAIN_TYREWALLS,        "Tire Walls" },
        { TERRAIN_CEMENTWALLS,      "Cement Walls" },
        { TERRAIN_GUARDRAILS,       "Guard Rails" },
        { TERRAIN_SAND,             "Sand" },
        { TERRAIN_BUMPY_SAND,       "Bumpy Sand" },
        { TERRAIN_DIRT,             "Dirt" },
        { TERRAIN_BUMPY_DIRT,       "Bumpy Dirt" },
        { TERRAIN_DIRT_ROAD,        "Dirt Road" },
        { TERRAIN_BUMPY_DIRT_ROAD,  "Bumpy Dirt Road" },
        { TERRAIN_PAVEMENT,         "Pavement" },
        { TERRAIN_DIRT_BANK,        "Dirt Bank" },
        { TERRAIN_WOOD,             "Wood" },
        { TERRAIN_DRY_VERGE,        "Dry Verge" },
        { TERRAIN_EXIT_RUMBLE_STRIPS, "Exit Rumble Strips" },
        { TERRAIN_GRASSCRETE,       "Grasscrete" },
        { TERRAIN_LONG_GRASS,       "Long Grass" },
        { TERRAIN_SLOPE_GRASS,      "Slope Grass" },
        { TERRAIN_COBBLES,          "Cobbles" },
        { TERRAIN_SAND_ROAD,        "Sand Road" },
        { TERRAIN_BAKED_CLAY,       "Baked Clay" },
        { TERRAIN_ASTROTURF,        "Astroturf" },
        { TERRAIN_SNOWHALF,         "Snow Half" },
        { TERRAIN_SNOWFULL,         "Snow Full" },
        { TERRAIN_DAMAGED_ROAD1,    "Damaged Road 1" },
        { TERRAIN_TRAIN_TRACK_ROAD, "Train Track Road" },
        { TERRAIN_BUMPYCOBBLES,     "Bumpy Cobbles" },
        { TERRAIN_ARIES_ONLY,       "Aries Only" },
        { TERRAIN_ORION_ONLY,       "Orion Only" },
        { TERRAIN_B1RUMBLES,        "B1 Rumbles" },
        { TERRAIN_B2RUMBLES,        "B2 Rumbles" },
        { TERRAIN_ROUGH_SAND_MEDIUM, "Rough Sand Medium" },
        { TERRAIN_ROUGH_SAND_HEAVY, "Rough Sand Heavy" },
        { TERRAIN_SNOWWALLS,        "Snow Walls" },
        { TERRAIN_ICE_ROAD,         "Ice Road" },
        { TERRAIN_RUNOFF_ROAD,      "Runoff Road" },
        { TERRAIN_ILLEGAL_STRIP,    "Illegal Strip" },
        { TERRAIN_PAINT_CONCRETE,   "Paint Concrete" },
        { TERRAIN_PAINT_CONCRETE_ILLEGAL, "Paint Concrete Illegal" },
        { TERRAIN_RALLY_TARMAC,     "Rally Tarmac" },
    };
}

string EncodeTerrainMaterial(int val)
{
    return lookup.at(val);
}
