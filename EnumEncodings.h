#pragma once

#include <string>
#include <unordered_map>

using namespace std;

class EnumEncodings
{
public:
    EnumEncodings()
        : m_encodings{}
    {
        PopulateLookup();
    }

    string Encode(int value)
    {
        return m_encodings.at(value);
    }

protected:
    virtual void PopulateLookup() = 0;
    unordered_map<int, string> m_encodings;
};

