#include "PitScheduleEncodings.h"

#include "SharedMemory.h"

#include <unordered_map>

using namespace std;

namespace
{
    const unordered_map<int, string> lookup{
        { PIT_SCHEDULE_NONE,                "None" },
        { PIT_SCHEDULE_PLAYER_REQUESTED,    "Player Requested" },
        { PIT_SCHEDULE_ENGINEER_REQUESTED,  "Engineer Requested" },
        { PIT_SCHEDULE_DAMAGE_REQUESTED,    "Damage Requested" },
        { PIT_SCHEDULE_MANDATORY,           "Mandatory" },
        { PIT_SCHEDULE_DRIVE_THROUGH,       "Drive Through" },
        { PIT_SCHEDULE_STOP_GO,             "Stop Go" },
        { PIT_SCHEDULE_PITSPOT_OCCUPIED,    "Pitspot Occupied" },
    };
}

string EncodePitSchedule(int val)
{
    return lookup.at(val);
}
