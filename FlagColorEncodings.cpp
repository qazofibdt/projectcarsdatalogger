#include "FlagColorEncodings.h"

#include "SharedMemory.h"

#include <unordered_map>

using namespace std;

namespace
{
    const unordered_map<int, string> lookup{
        { FLAG_COLOUR_NONE,             "None" },
        { FLAG_COLOUR_GREEN,            "Green" },
        { FLAG_COLOUR_BLUE,             "Blue" },
        { FLAG_COLOUR_WHITE_SLOW_CAR,   "White Slow Car" },
        { FLAG_COLOUR_WHITE_FINAL_LAP,  "White Final Lap" },
        { FLAG_COLOUR_RED,              "Red" },
        { FLAG_COLOUR_YELLOW,           "Yellow" },
        { FLAG_COLOUR_DOUBLE_YELLOW,    "Double Yellow" },
        { FLAG_COLOUR_BLACK_AND_WHITE,  "Black and White" },
        { FLAG_COLOUR_BLACK_ORANGE_CIRCLE, "Black Orange Circle" },
        { FLAG_COLOUR_BLACK,            "Black" },
        { FLAG_COLOUR_CHEQUERED,        "Chequered" },
    };
}

string EncodeFlagColor(int val)
{
    return lookup.at(val);
}
