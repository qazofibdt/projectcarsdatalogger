#pragma once

#include "SharedMemory.h"
#include <memory>

using Telemetry = SharedMemory;

namespace
{
    struct TelemetryPollerResources;
}

using namespace std;

class TelemetryPoller
{
public:
    TelemetryPoller();
    ~TelemetryPoller();
    unique_ptr<Telemetry> Next();

private:
    unique_ptr<::TelemetryPollerResources> m_resources;
};