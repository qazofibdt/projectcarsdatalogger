#include "FlagReasonEncodings.h"

#include "SharedMemory.h"

#include <unordered_map>

using namespace std;

namespace
{
    const unordered_map<int, string> lookup{
        { FLAG_REASON_NONE,             "None" },
        { FLAG_REASON_SOLO_CRASH,       "Solo Crash" },
        { FLAG_REASON_VEHICLE_CRASH,    "Vehicle Crash" },
        { FLAG_REASON_VEHICLE_OBSTRUCTION, "Vehicle Obstruction" },
    };
}

string EncodeFlagReason(int val)
{
    return lookup.at(val);
}
