#include "RaceStateEncodings.h"

#include "SharedMemory.h"

#include <unordered_map>

using namespace std;

namespace
{
    const unordered_map<int, string> lookup{
        { RACESTATE_INVALID,        "Invalid" },
        { RACESTATE_NOT_STARTED,    "Not Started" },
        { RACESTATE_RACING,         "Racing" },
        { RACESTATE_FINISHED,       "Finished" },
        { RACESTATE_DISQUALIFIED,   "Disqualified" },
        { RACESTATE_RETIRED,        "Retired" },
        { RACESTATE_DNF,            "DNF" },
    };
}

string EncodeRaceState(int val)
{
    return lookup.at(val);
}
