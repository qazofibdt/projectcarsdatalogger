#include "CrashStateEncodings.h"

#include "SharedMemory.h"

#include <unordered_map>

using namespace std;

namespace
{
    const unordered_map<int, string> lookup{
        { CRASH_DAMAGE_NONE,        "None" },
        { CRASH_DAMAGE_OFFTRACK,    "Offtrack" },
        { CRASH_DAMAGE_LARGE_PROP,  "Large Prop" },
        { CRASH_DAMAGE_SPINNING,    "Spinning" },
        { CRASH_DAMAGE_ROLLING,     "Rolling" },
    };
}

string EncodeCrashState(int val)
{
    return lookup.at(val);
}
