// For UUIDs
#pragma comment(lib, "rpcrt4.lib")

#include "SharedMemory.h"

#include "CSVFile.h"
#include "TelemetryPoller.h"

#include "CrashStateEncodings.h"
#include "EnumEncodings.h"
#include "FlagColorEncodings.h"
#include "FlagReasonEncodings.h"
#include "GameStateEncodings.h"
#include "PitModeEncodings.h"
#include "PitScheduleEncodings.h"
#include "RaceStateEncodings.h"
#include "SessionStateEncodings.h"
#include "TerrainMaterialEncodings.h"

#include <functional>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <vector>
#include <queue>

// For UUIDs
#include <windows.h>
// For keyboard input
#include <conio.h>
// For SetConsoleCursorPosition
#include <consoleapi.h>

using namespace std;

namespace
{
    void SetConsoleCursorPosition(short x, short y)
    {
        HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
        COORD position = { x, y };

        SetConsoleCursorPosition(hStdout, position);
    }

    template<typename T>
    string ToString(const T& item)
    {
        ostringstream stream;
        stream << item;
        return stream.str();
    }

    template<>
    string ToString(const float& item)
    {
        ostringstream stream;
        // Always output the decimal point
        stream << showpoint << fixed;
        stream << item;
        return stream.str();
    }

    template<>
    string ToString(const UUID& uuid)
    {
        char* cstr;
        UuidToStringA(&uuid, (RPC_CSTR*)&cstr);
        string str{ cstr };
        RpcStringFreeA((RPC_CSTR*)&cstr);
        return str;
    }

    template<>
    string ToString(const bool& b)
    {
        if (b)
            return "true";
        else
            return "false";
    }

    template<typename T>
    string ToString(const T& item, function<bool(const T&)> nullPredicate)
    {
        if (nullPredicate(item))
        {
            return ToString(item);
        }
        else
        {
            return "";
        }
    }

    template<typename T>
    string ToString(const T& item, const T* nullVal)
    {
        return ToString<T>(item, [&](const T& val) -> bool { return nullVal == nullptr || val != *nullVal; });
    }

    // Explicit specialization for floats, for fuzzy comparison to the null value
    template<>
    string ToString(const float& item, const float* nullVal)
    {
        return ToString<float>(item, [&](const float& val) -> bool { return nullVal == nullptr || (abs(val - *nullVal) > FLT_EPSILON); });
    }

    template<typename T>
    string ToString(const T& item, const T& nullVal)
    {
        return ToString(item, &nullVal);
    }

    template<typename T>
    string ToString(const T& item, T&& nullVal)
    {
        return ToString(item, &nullVal);
    }

    const ParticipantInfo* GetCurrentParticipantInfo(const Telemetry& telemetry)
    {
        const bool isValidParticipantIndex =
            telemetry.mViewedParticipantIndex != -1
            && telemetry.mViewedParticipantIndex < telemetry.mNumParticipants
            && telemetry.mViewedParticipantIndex < STORED_PARTICIPANTS_MAX;

        if (!isValidParticipantIndex)
            return nullptr;

        return &telemetry.mParticipantInfo[telemetry.mViewedParticipantIndex];
    }
}

const vector<pair<string, function<string(const Telemetry&)>>> COLUMNS{
    { "Version",                     [](const Telemetry& telemetry) { return ToString(telemetry.mVersion); } },
    { "BuildVersionNumber",          [](const Telemetry& telemetry) { return ToString(telemetry.mBuildVersionNumber, (unsigned int)0); } },

    { "GameState",                   [](const Telemetry& telemetry) { return EncodeGameState(telemetry.mGameState); } },
    { "SessionState",                [](const Telemetry& telemetry) { return EncodeSessionState(telemetry.mSessionState); } },
    { "RaceState",                   [](const Telemetry& telemetry) { return EncodeRaceState(telemetry.mRaceState); } },

    { "ViewedParticipantIndex",      [](const Telemetry& telemetry) { return ToString(telemetry.mViewedParticipantIndex, -1); } },
    { "NumParticipants",             [](const Telemetry& telemetry) { return ToString(telemetry.mNumParticipants, -1); } },

    { "ParticipantIsActive",         [](const Telemetry& telemetry) {
        auto info = GetCurrentParticipantInfo(telemetry);
        return (info == nullptr) ? "" : ToString(info->mIsActive);
    } },
    { "ParticipantName",             [](const Telemetry& telemetry) {
        auto info = GetCurrentParticipantInfo(telemetry);
        return (info == nullptr) ? "" : ToString(info->mName);
    } },
    { "ParticipantWorldPositionX",   [](const Telemetry& telemetry) {
        auto info = GetCurrentParticipantInfo(telemetry);
        return (info == nullptr) ? "" : ToString(info->mWorldPosition[VEC_X], 0.0f); // For all world positions, 1 unit = 1 meter
    } },
    { "ParticipantWorldPositionY",   [](const Telemetry& telemetry) {
        auto info = GetCurrentParticipantInfo(telemetry);
        return (info == nullptr) ? "" : ToString(info->mWorldPosition[VEC_Y], 0.0f);
    } },
    { "ParticipantWorldPositionZ",   [](const Telemetry& telemetry) {
        auto info = GetCurrentParticipantInfo(telemetry);
        return (info == nullptr) ? "" : ToString(info->mWorldPosition[VEC_Z], 0.0f);
    } },
    { "ParticipantCurrentLapDistance", [](const Telemetry& telemetry) {
        auto info = GetCurrentParticipantInfo(telemetry);
        return (info == nullptr) ? "" : ToString(info->mCurrentLapDistance);
    } },
    { "ParticipantRacePosition",     [](const Telemetry& telemetry) {
        auto info = GetCurrentParticipantInfo(telemetry);
        return (info == nullptr) ? "" : ToString(info->mRacePosition, (unsigned int)0);
    } },
    { "ParticipantLapsCompleted",    [](const Telemetry& telemetry) {
        auto info = GetCurrentParticipantInfo(telemetry);
        return (info == nullptr) ? "" : ToString(info->mLapsCompleted);
    } },
    { "ParticipantCurrentLap",       [](const Telemetry& telemetry) {
        auto info = GetCurrentParticipantInfo(telemetry);
        return (info == nullptr) ? "" : ToString(info->mCurrentLap);
    } },
    { "ParticipantCurrentSector",    [](const Telemetry& telemetry) {
        auto info = GetCurrentParticipantInfo(telemetry);
        return (info == nullptr) ? "" : ToString(info->mCurrentSector, -1);
    } },

    { "UnfilteredThrottle",         [](const Telemetry& telemetry) { return ToString(telemetry.mUnfilteredThrottle); } },
    { "UnfilteredBrake",            [](const Telemetry& telemetry) { return ToString(telemetry.mUnfilteredBrake); } },
    { "UnfilteredSteering",         [](const Telemetry& telemetry) { return ToString(telemetry.mUnfilteredSteering); } },
    { "UnfilteredClutch",           [](const Telemetry& telemetry) { return ToString(telemetry.mUnfilteredClutch); } },

    { "CarName",                    [](const Telemetry& telemetry) { return ToString(telemetry.mCarName); } },
    { "CarClassName",               [](const Telemetry& telemetry) { return ToString(telemetry.mCarClassName); } },

    { "LapsInEvent",                [](const Telemetry& telemetry) { return ToString(telemetry.mLapsInEvent); } }, // 0 --> null?
    { "TrackLocation",              [](const Telemetry& telemetry) { return ToString(telemetry.mTrackLocation); } },
    { "TrackVariation",             [](const Telemetry& telemetry) { return ToString(telemetry.mTrackVariation); } },
    { "TrackLength",                [](const Telemetry& telemetry) { return ToString(telemetry.mTrackLength, 0.0f); } },

    { "NumSectors",                 [](const Telemetry& telemetry) { return ToString(telemetry.mNumSectors, -1); } },
    { "LapInvalidated",             [](const Telemetry& telemetry) { return ToString(telemetry.mLapInvalidated); } },
    { "BestLapTime",                [](const Telemetry& telemetry) { return ToString(telemetry.mBestLapTime, -1.0f); } },
    { "LastLapTime",                [](const Telemetry& telemetry) { return ToString(telemetry.mLastLapTime, -1.0f); } },
    { "CurrentTime",                [](const Telemetry& telemetry) { return ToString(telemetry.mCurrentTime, -1.0f); } },
    { "SplitTimeAhead",             [](const Telemetry& telemetry) { return ToString(telemetry.mSplitTimeAhead, -1.0f); } },
    { "SplitTimeBehind",            [](const Telemetry& telemetry) { return ToString(telemetry.mSplitTimeBehind, -1.0f); } },
    { "SplitTime",                  [](const Telemetry& telemetry) { return ToString(telemetry.mSplitTime); } }, // -1 --> null?
    { "EventTimeRemaining",         [](const Telemetry& telemetry) { return ToString(telemetry.mEventTimeRemaining, -1.0f); } },
    { "PersonalFastestLapTime",     [](const Telemetry& telemetry) { return ToString(telemetry.mPersonalFastestLapTime, -1.0f); } },
    { "WorldFastestLapTime",        [](const Telemetry& telemetry) { return ToString(telemetry.mWorldFastestLapTime, -1.0f); } },
    { "CurrentSector1Time",         [](const Telemetry& telemetry) { return ToString(telemetry.mCurrentSector1Time, -1.0f); } },
    { "CurrentSector2Time",         [](const Telemetry& telemetry) { return ToString(telemetry.mCurrentSector2Time, -1.0f); } },
    { "CurrentSector3Time",         [](const Telemetry& telemetry) { return ToString(telemetry.mCurrentSector3Time, -1.0f); } },
    { "FastestSector1Time",         [](const Telemetry& telemetry) { return ToString(telemetry.mFastestSector1Time, -1.0f); } },
    { "FastestSector2Time",         [](const Telemetry& telemetry) { return ToString(telemetry.mFastestSector2Time, -1.0f); } },
    { "FastestSector3Time",         [](const Telemetry& telemetry) { return ToString(telemetry.mFastestSector3Time, -1.0f); } },
    { "PersonalFastestSector1Time", [](const Telemetry& telemetry) { return ToString(telemetry.mPersonalFastestSector1Time, -1.0f); } },
    { "PersonalFastestSector2Time", [](const Telemetry& telemetry) { return ToString(telemetry.mPersonalFastestSector2Time, -1.0f); } },
    { "PersonalFastestSector3Time", [](const Telemetry& telemetry) { return ToString(telemetry.mPersonalFastestSector3Time, -1.0f); } },
    { "WorldFastestSector1Time",    [](const Telemetry& telemetry) { return ToString(telemetry.mWorldFastestSector1Time, -1.0f); } },
    { "WorldFastestSector2Time",    [](const Telemetry& telemetry) { return ToString(telemetry.mWorldFastestSector2Time, -1.0f); } },
    { "WorldFastestSector3Time",    [](const Telemetry& telemetry) { return ToString(telemetry.mWorldFastestSector3Time, -1.0f); } },

    { "HighestFlagColour",          [](const Telemetry& telemetry) { return EncodeFlagColor(telemetry.mHighestFlagColour); } },
    { "HighestFlagReason",          [](const Telemetry& telemetry) { return EncodeFlagReason(telemetry.mHighestFlagReason); } },

    { "PitMode",                    [](const Telemetry& telemetry) { return EncodePitMode(telemetry.mPitMode); } },
    { "PitSchedule",                [](const Telemetry& telemetry) { return EncodePitSchedule(telemetry.mPitSchedule); } },

    { "CarHeadlight",               [](const Telemetry& telemetry) { return ToString((telemetry.mCarFlags & CAR_HEADLIGHT) != 0); } },
    { "CarEngineActive",            [](const Telemetry& telemetry) { return ToString((telemetry.mCarFlags & CAR_ENGINE_ACTIVE) != 0); } },
    { "CarEngineWarning",           [](const Telemetry& telemetry) { return ToString((telemetry.mCarFlags & CAR_ENGINE_WARNING) != 0); } },
    { "CarSpeedLimiter",            [](const Telemetry& telemetry) { return ToString((telemetry.mCarFlags & CAR_SPEED_LIMITER) != 0); } },
    { "CarABS",                     [](const Telemetry& telemetry) { return ToString((telemetry.mCarFlags & CAR_ABS) != 0); } },
    { "CarHandbrake",               [](const Telemetry& telemetry) { return ToString((telemetry.mCarFlags & CAR_HANDBRAKE) != 0); } },
    { "OilTempCelsius",             [](const Telemetry& telemetry) { return ToString(telemetry.mOilTempCelsius); } },
    { "OilPressureKPa",             [](const Telemetry& telemetry) { return ToString(telemetry.mOilPressureKPa); } },
    { "WaterTempCelsius",           [](const Telemetry& telemetry) { return ToString(telemetry.mWaterTempCelsius); } },
    { "WaterPressureKPa",           [](const Telemetry& telemetry) { return ToString(telemetry.mWaterPressureKPa); } },
    { "FuelPressureKPa",            [](const Telemetry& telemetry) { return ToString(telemetry.mFuelPressureKPa); } },
    { "FuelLevel",                  [](const Telemetry& telemetry) { return ToString(telemetry.mFuelLevel); } },
    { "FuelCapacity",               [](const Telemetry& telemetry) { return ToString(telemetry.mFuelCapacity); } },
    { "Speed",                      [](const Telemetry& telemetry) { return ToString(telemetry.mSpeed); } },
    { "Rpm",                        [](const Telemetry& telemetry) { return ToString(telemetry.mRpm); } },
    { "MaxRPM",                     [](const Telemetry& telemetry) { return ToString(telemetry.mMaxRPM); } },
    { "Brake",                      [](const Telemetry& telemetry) { return ToString(telemetry.mBrake); } },
    { "Throttle",                   [](const Telemetry& telemetry) { return ToString(telemetry.mThrottle); } },
    { "Clutch",                     [](const Telemetry& telemetry) { return ToString(telemetry.mClutch); } },
    { "Steering",                   [](const Telemetry& telemetry) { return ToString(telemetry.mSteering); } },
    { "Gear",                       [](const Telemetry& telemetry) { return ToString(telemetry.mGear); } },
    { "NumGears",                   [](const Telemetry& telemetry) { return ToString(telemetry.mNumGears, -1); } },
    { "OdometerKM",                 [](const Telemetry& telemetry) { return ToString(telemetry.mOdometerKM, -1.0f); } },
    { "AntiLockActive",             [](const Telemetry& telemetry) { return ToString(telemetry.mAntiLockActive); } },
    { "LastOpponentCollisionIndex", [](const Telemetry& telemetry) { return ToString(telemetry.mLastOpponentCollisionIndex, -1); } },
    { "LastOpponentCollisionMagnitude", [](const Telemetry& telemetry) { return ToString(telemetry.mLastOpponentCollisionMagnitude); } },
    { "BoostActive",                [](const Telemetry& telemetry) { return ToString(telemetry.mBoostActive); } },
    { "BoostAmount",                [](const Telemetry& telemetry) { return ToString(telemetry.mBoostAmount); } },

    { "OrientationX",               [](const Telemetry& telemetry) { return ToString(telemetry.mOrientation[VEC_X]); } },
    { "OrientationY",               [](const Telemetry& telemetry) { return ToString(telemetry.mOrientation[VEC_Y]); } },
    { "OrientationZ",               [](const Telemetry& telemetry) { return ToString(telemetry.mOrientation[VEC_Z]); } },
    { "LocalVelocityX",             [](const Telemetry& telemetry) { return ToString(telemetry.mLocalVelocity[VEC_X]); } },
    { "LocalVelocityY",             [](const Telemetry& telemetry) { return ToString(telemetry.mLocalVelocity[VEC_Y]); } },
    { "LocalVelocityZ",             [](const Telemetry& telemetry) { return ToString(telemetry.mLocalVelocity[VEC_Z]); } },
    { "WorldVelocityX",             [](const Telemetry& telemetry) { return ToString(telemetry.mWorldVelocity[VEC_X]); } },
    { "WorldVelocityY",             [](const Telemetry& telemetry) { return ToString(telemetry.mWorldVelocity[VEC_Y]); } },
    { "WorldVelocityZ",             [](const Telemetry& telemetry) { return ToString(telemetry.mWorldVelocity[VEC_Z]); } },
    { "AngularVelocityX",           [](const Telemetry& telemetry) { return ToString(telemetry.mAngularVelocity[VEC_X]); } },
    { "AngularVelocityY",           [](const Telemetry& telemetry) { return ToString(telemetry.mAngularVelocity[VEC_Y]); } },
    { "AngularVelocityZ",           [](const Telemetry& telemetry) { return ToString(telemetry.mAngularVelocity[VEC_Z]); } },
    { "LocalAccelerationX",         [](const Telemetry& telemetry) { return ToString(telemetry.mLocalAcceleration[VEC_X]); } },
    { "LocalAccelerationY",         [](const Telemetry& telemetry) { return ToString(telemetry.mLocalAcceleration[VEC_Y]); } },
    { "LocalAccelerationZ",         [](const Telemetry& telemetry) { return ToString(telemetry.mLocalAcceleration[VEC_Z]); } },
    { "WorldAccelerationX",         [](const Telemetry& telemetry) { return ToString(telemetry.mWorldAcceleration[VEC_X]); } },
    { "WorldAccelerationY",         [](const Telemetry& telemetry) { return ToString(telemetry.mWorldAcceleration[VEC_Y]); } },
    { "WorldAccelerationZ",         [](const Telemetry& telemetry) { return ToString(telemetry.mWorldAcceleration[VEC_Z]); } },
    { "ExtentsCentreX",             [](const Telemetry& telemetry) { return ToString(telemetry.mExtentsCentre[VEC_X]); } },
    { "ExtentsCentreY",             [](const Telemetry& telemetry) { return ToString(telemetry.mExtentsCentre[VEC_Y]); } },
    { "ExtentsCentreZ",             [](const Telemetry& telemetry) { return ToString(telemetry.mExtentsCentre[VEC_Z]); } },

    { "TyreAttachedFR",             [](const Telemetry& telemetry) { return ToString((telemetry.mTyreFlags[TYRE_FRONT_RIGHT] & TYRE_ATTACHED) != 0); } },
    { "TyreInflatedFR",             [](const Telemetry& telemetry) { return ToString((telemetry.mTyreFlags[TYRE_FRONT_RIGHT] & TYRE_INFLATED) != 0); } },
    { "TyreIsOnGroundFR",           [](const Telemetry& telemetry) { return ToString((telemetry.mTyreFlags[TYRE_FRONT_RIGHT] & TYRE_IS_ON_GROUND) != 0); } },
    { "TyreAttachedFL",             [](const Telemetry& telemetry) { return ToString((telemetry.mTyreFlags[TYRE_FRONT_LEFT] & TYRE_ATTACHED) != 0); } },
    { "TyreInflatedFL",             [](const Telemetry& telemetry) { return ToString((telemetry.mTyreFlags[TYRE_FRONT_LEFT] & TYRE_INFLATED) != 0); } },
    { "TyreIsOnGroundFL",           [](const Telemetry& telemetry) { return ToString((telemetry.mTyreFlags[TYRE_FRONT_LEFT] & TYRE_IS_ON_GROUND) != 0); } },
    { "TyreAttachedRR",             [](const Telemetry& telemetry) { return ToString((telemetry.mTyreFlags[TYRE_REAR_RIGHT] & TYRE_ATTACHED) != 0); } },
    { "TyreInflatedRR",             [](const Telemetry& telemetry) { return ToString((telemetry.mTyreFlags[TYRE_REAR_RIGHT] & TYRE_INFLATED) != 0); } },
    { "TyreIsOnGroundRR",           [](const Telemetry& telemetry) { return ToString((telemetry.mTyreFlags[TYRE_REAR_RIGHT] & TYRE_IS_ON_GROUND) != 0); } },
    { "TyreAttachedRL",             [](const Telemetry& telemetry) { return ToString((telemetry.mTyreFlags[TYRE_REAR_LEFT] & TYRE_ATTACHED) != 0); } },
    { "TyreInflatedRL",             [](const Telemetry& telemetry) { return ToString((telemetry.mTyreFlags[TYRE_REAR_LEFT] & TYRE_INFLATED) != 0); } },
    { "TyreIsOnGroundRL",           [](const Telemetry& telemetry) { return ToString((telemetry.mTyreFlags[TYRE_REAR_LEFT] & TYRE_IS_ON_GROUND) != 0); } },
    { "TerrainFR",                  [](const Telemetry& telemetry) { return EncodeTerrainMaterial(telemetry.mTerrain[TYRE_FRONT_RIGHT]); } },
    { "TerrainFL",                  [](const Telemetry& telemetry) { return EncodeTerrainMaterial(telemetry.mTerrain[TYRE_FRONT_LEFT]); } },
    { "TerrainRR",                  [](const Telemetry& telemetry) { return EncodeTerrainMaterial(telemetry.mTerrain[TYRE_REAR_RIGHT]); } },
    { "TerrainRL",                  [](const Telemetry& telemetry) { return EncodeTerrainMaterial(telemetry.mTerrain[TYRE_REAR_LEFT]); } },
    { "TyreYFR",                    [](const Telemetry& telemetry) { return ToString(telemetry.mTyreY[TYRE_FRONT_RIGHT]); } },
    { "TyreYFL",                    [](const Telemetry& telemetry) { return ToString(telemetry.mTyreY[TYRE_FRONT_LEFT]); } },
    { "TyreYRR",                    [](const Telemetry& telemetry) { return ToString(telemetry.mTyreY[TYRE_REAR_RIGHT]); } },
    { "TyreYRL",                    [](const Telemetry& telemetry) { return ToString(telemetry.mTyreY[TYRE_REAR_LEFT]); } },
    { "TyreRPSFR",                  [](const Telemetry& telemetry) { return ToString(telemetry.mTyreRPS[TYRE_FRONT_RIGHT]); } },
    { "TyreRPSFL",                  [](const Telemetry& telemetry) { return ToString(telemetry.mTyreRPS[TYRE_FRONT_LEFT]); } },
    { "TyreRPSRR",                  [](const Telemetry& telemetry) { return ToString(telemetry.mTyreRPS[TYRE_REAR_RIGHT]); } },
    { "TyreRPSRL",                  [](const Telemetry& telemetry) { return ToString(telemetry.mTyreRPS[TYRE_REAR_LEFT]); } },
    { "TyreTempFR",                 [](const Telemetry& telemetry) { return ToString(telemetry.mTyreTemp[TYRE_FRONT_RIGHT]); } },
    { "TyreTempFL",                 [](const Telemetry& telemetry) { return ToString(telemetry.mTyreTemp[TYRE_FRONT_LEFT]); } },
    { "TyreTempRR",                 [](const Telemetry& telemetry) { return ToString(telemetry.mTyreTemp[TYRE_REAR_RIGHT]); } },
    { "TyreTempRL",                 [](const Telemetry& telemetry) { return ToString(telemetry.mTyreTemp[TYRE_REAR_LEFT]); } },
    { "TyreHeightAboveGroundFR",    [](const Telemetry& telemetry) { return ToString(telemetry.mTyreHeightAboveGround[TYRE_FRONT_RIGHT]); } },
    { "TyreHeightAboveGroundFL",    [](const Telemetry& telemetry) { return ToString(telemetry.mTyreHeightAboveGround[TYRE_FRONT_LEFT]); } },
    { "TyreHeightAboveGroundRR",    [](const Telemetry& telemetry) { return ToString(telemetry.mTyreHeightAboveGround[TYRE_REAR_RIGHT]); } },
    { "TyreHeightAboveGroundRL",    [](const Telemetry& telemetry) { return ToString(telemetry.mTyreHeightAboveGround[TYRE_REAR_LEFT]); } },
    { "TyreWearFR",                 [](const Telemetry& telemetry) { return ToString(telemetry.mTyreWear[TYRE_FRONT_RIGHT]); } },
    { "TyreWearFL",                 [](const Telemetry& telemetry) { return ToString(telemetry.mTyreWear[TYRE_FRONT_LEFT]); } },
    { "TyreWearRR",                 [](const Telemetry& telemetry) { return ToString(telemetry.mTyreWear[TYRE_REAR_RIGHT]); } },
    { "TyreWearRL",                 [](const Telemetry& telemetry) { return ToString(telemetry.mTyreWear[TYRE_REAR_LEFT]); } },
    { "BrakeDamageFR",              [](const Telemetry& telemetry) { return ToString(telemetry.mBrakeDamage[TYRE_FRONT_RIGHT]); } },
    { "BrakeDamageFL",              [](const Telemetry& telemetry) { return ToString(telemetry.mBrakeDamage[TYRE_FRONT_LEFT]); } },
    { "BrakeDamageRR",              [](const Telemetry& telemetry) { return ToString(telemetry.mBrakeDamage[TYRE_REAR_RIGHT]); } },
    { "BrakeDamageRL",              [](const Telemetry& telemetry) { return ToString(telemetry.mBrakeDamage[TYRE_REAR_LEFT]); } },
    { "SuspensionDamageFR",         [](const Telemetry& telemetry) { return ToString(telemetry.mSuspensionDamage[TYRE_FRONT_RIGHT]); } },
    { "SuspensionDamageFL",         [](const Telemetry& telemetry) { return ToString(telemetry.mSuspensionDamage[TYRE_FRONT_LEFT]); } },
    { "SuspensionDamageRR",         [](const Telemetry& telemetry) { return ToString(telemetry.mSuspensionDamage[TYRE_REAR_RIGHT]); } },
    { "SuspensionDamageRL",         [](const Telemetry& telemetry) { return ToString(telemetry.mSuspensionDamage[TYRE_REAR_LEFT]); } },
    { "BrakeTempCelsiusFR",         [](const Telemetry& telemetry) { return ToString(telemetry.mBrakeTempCelsius[TYRE_FRONT_RIGHT]); } },
    { "BrakeTempCelsiusFL",         [](const Telemetry& telemetry) { return ToString(telemetry.mBrakeTempCelsius[TYRE_FRONT_LEFT]); } },
    { "BrakeTempCelsiusRR",         [](const Telemetry& telemetry) { return ToString(telemetry.mBrakeTempCelsius[TYRE_REAR_RIGHT]); } },
    { "BrakeTempCelsiusRL",         [](const Telemetry& telemetry) { return ToString(telemetry.mBrakeTempCelsius[TYRE_REAR_LEFT]); } },
    { "TyreTreadTempFR",            [](const Telemetry& telemetry) { return ToString(telemetry.mTyreTreadTemp[TYRE_FRONT_RIGHT]); } },
    { "TyreTreadTempFL",            [](const Telemetry& telemetry) { return ToString(telemetry.mTyreTreadTemp[TYRE_FRONT_LEFT]); } },
    { "TyreTreadTempRR",            [](const Telemetry& telemetry) { return ToString(telemetry.mTyreTreadTemp[TYRE_REAR_RIGHT]); } },
    { "TyreTreadTempRL",            [](const Telemetry& telemetry) { return ToString(telemetry.mTyreTreadTemp[TYRE_REAR_LEFT]); } },
    { "TyreLayerTempFR",            [](const Telemetry& telemetry) { return ToString(telemetry.mTyreLayerTemp[TYRE_FRONT_RIGHT]); } },
    { "TyreLayerTempFL",            [](const Telemetry& telemetry) { return ToString(telemetry.mTyreLayerTemp[TYRE_FRONT_LEFT]); } },
    { "TyreLayerTempRR",            [](const Telemetry& telemetry) { return ToString(telemetry.mTyreLayerTemp[TYRE_REAR_RIGHT]); } },
    { "TyreLayerTempRL",            [](const Telemetry& telemetry) { return ToString(telemetry.mTyreLayerTemp[TYRE_REAR_LEFT]); } },
    { "TyreCarcassTempFR",          [](const Telemetry& telemetry) { return ToString(telemetry.mTyreCarcassTemp[TYRE_FRONT_RIGHT]); } },
    { "TyreCarcassTempFL",          [](const Telemetry& telemetry) { return ToString(telemetry.mTyreCarcassTemp[TYRE_FRONT_LEFT]); } },
    { "TyreCarcassTempRR",          [](const Telemetry& telemetry) { return ToString(telemetry.mTyreCarcassTemp[TYRE_REAR_RIGHT]); } },
    { "TyreCarcassTempRL",          [](const Telemetry& telemetry) { return ToString(telemetry.mTyreCarcassTemp[TYRE_REAR_LEFT]); } },
    { "TyreRimTempFR",              [](const Telemetry& telemetry) { return ToString(telemetry.mTyreRimTemp[TYRE_FRONT_RIGHT]); } },
    { "TyreRimTempFL",              [](const Telemetry& telemetry) { return ToString(telemetry.mTyreRimTemp[TYRE_FRONT_LEFT]); } },
    { "TyreRimTempRR",              [](const Telemetry& telemetry) { return ToString(telemetry.mTyreRimTemp[TYRE_REAR_RIGHT]); } },
    { "TyreRimTempRL",              [](const Telemetry& telemetry) { return ToString(telemetry.mTyreRimTemp[TYRE_REAR_LEFT]); } },
    { "TyreInternalAirTempFR",      [](const Telemetry& telemetry) { return ToString(telemetry.mTyreInternalAirTemp[TYRE_FRONT_RIGHT]); } },
    { "TyreInternalAirTempFL",      [](const Telemetry& telemetry) { return ToString(telemetry.mTyreInternalAirTemp[TYRE_FRONT_LEFT]); } },
    { "TyreInternalAirTempRR",      [](const Telemetry& telemetry) { return ToString(telemetry.mTyreInternalAirTemp[TYRE_REAR_RIGHT]); } },
    { "TyreInternalAirTempRL",      [](const Telemetry& telemetry) { return ToString(telemetry.mTyreInternalAirTemp[TYRE_REAR_LEFT]); } },

    { "CrashState",                 [](const Telemetry& telemetry) { return EncodeCrashState(telemetry.mCrashState); } },
    { "AeroDamage",                 [](const Telemetry& telemetry) { return ToString(telemetry.mAeroDamage); } },
    { "EngineDamage",               [](const Telemetry& telemetry) { return ToString(telemetry.mEngineDamage); } },

    { "AmbientTemperature",         [](const Telemetry& telemetry) { return ToString(telemetry.mAmbientTemperature); } },
    { "TrackTemperature",           [](const Telemetry& telemetry) { return ToString(telemetry.mTrackTemperature); } },
    { "RainDensity",                [](const Telemetry& telemetry) { return ToString(telemetry.mRainDensity); } },
    { "WindSpeed",                  [](const Telemetry& telemetry) { return ToString(telemetry.mWindSpeed); } },
    { "WindDirectionX",             [](const Telemetry& telemetry) { return ToString(telemetry.mWindDirectionX); } },
    { "WindDirectionY",             [](const Telemetry& telemetry) { return ToString(telemetry.mWindDirectionY); } },
    { "CloudBrightness",            [](const Telemetry& telemetry) { return ToString(telemetry.mCloudBrightness); } },

    { "SequenceNumber",             [](const Telemetry& telemetry) { return ToString(telemetry.mSequenceNumber); } },

    { "WheelLocalPositionYFR",      [](const Telemetry& telemetry) { return ToString(telemetry.mWheelLocalPositionY[TYRE_FRONT_LEFT]); } },
    { "WheelLocalPositionYFL",      [](const Telemetry& telemetry) { return ToString(telemetry.mWheelLocalPositionY[TYRE_FRONT_LEFT]); } },
    { "WheelLocalPositionYRR",      [](const Telemetry& telemetry) { return ToString(telemetry.mWheelLocalPositionY[TYRE_REAR_RIGHT]); } },
    { "WheelLocalPositionYRL",      [](const Telemetry& telemetry) { return ToString(telemetry.mWheelLocalPositionY[TYRE_REAR_LEFT]); } },
    { "SuspensionTravelFR",         [](const Telemetry& telemetry) { return ToString(telemetry.mSuspensionTravel[TYRE_FRONT_RIGHT]); } },
    { "SuspensionTravelFL",         [](const Telemetry& telemetry) { return ToString(telemetry.mSuspensionTravel[TYRE_FRONT_LEFT]); } },
    { "SuspensionTravelRR",         [](const Telemetry& telemetry) { return ToString(telemetry.mSuspensionTravel[TYRE_REAR_RIGHT]); } },
    { "SuspensionTravelRL",         [](const Telemetry& telemetry) { return ToString(telemetry.mSuspensionTravel[TYRE_REAR_LEFT]); } },
    { "SuspensionVelocityFR",       [](const Telemetry& telemetry) { return ToString(telemetry.mSuspensionVelocity[TYRE_FRONT_RIGHT]); } },
    { "SuspensionVelocityFL",       [](const Telemetry& telemetry) { return ToString(telemetry.mSuspensionVelocity[TYRE_FRONT_LEFT]); } },
    { "SuspensionVelocityRR",       [](const Telemetry& telemetry) { return ToString(telemetry.mSuspensionVelocity[TYRE_REAR_RIGHT]); } },
    { "SuspensionVelocityRL",       [](const Telemetry& telemetry) { return ToString(telemetry.mSuspensionVelocity[TYRE_REAR_LEFT]); } },
    { "AirPressureFR",              [](const Telemetry& telemetry) { return ToString(telemetry.mAirPressure[TYRE_FRONT_RIGHT]); } },
    { "AirPressureFL",              [](const Telemetry& telemetry) { return ToString(telemetry.mAirPressure[TYRE_FRONT_LEFT]); } },
    { "AirPressureRR",              [](const Telemetry& telemetry) { return ToString(telemetry.mAirPressure[TYRE_REAR_RIGHT]); } },
    { "AirPressureRL",              [](const Telemetry& telemetry) { return ToString(telemetry.mAirPressure[TYRE_REAR_LEFT]); } },
    { "EngineSpeed",                [](const Telemetry& telemetry) { return ToString(telemetry.mEngineSpeed); } },
    { "EngineTorque",               [](const Telemetry& telemetry) { return ToString(telemetry.mEngineTorque); } },
    { "WingF",                      [](const Telemetry& telemetry) { return ToString(telemetry.mWings[0]); } }, // TODO verify that 0 is front
    { "WingR",                      [](const Telemetry& telemetry) { return ToString(telemetry.mWings[1]); } },
    { "HandBrake",                  [](const Telemetry& telemetry) { return ToString(telemetry.mHandBrake); } },

    // The API returns other participants' data in the same table. For our purposes, we need to split it up into
    // one table per participant. Just skipping that data for now

    { "EnforcedPitStopLap",          [](const Telemetry& telemetry) { return ToString(telemetry.mEnforcedPitStopLap, -1); } },
    { "TranslatedTrackLocation",     [](const Telemetry& telemetry) { return ToString(telemetry.mTranslatedTrackLocation); } },
    { "TranslatedTrackVariation",    [](const Telemetry& telemetry) { return ToString(telemetry.mTranslatedTrackVariation); } },
    { "BrakeBias",                   [](const Telemetry& telemetry) { return ToString(telemetry.mBrakeBias); } },
    { "TurboBoostPressure",          [](const Telemetry& telemetry) { return ToString(telemetry.mTurboBoostPressure); } },

    { "TyreCompoundFR",              [](const Telemetry& telemetry) { return ToString(telemetry.mTyreCompound[TYRE_FRONT_RIGHT]); } },
    { "TyreCompoundFL",              [](const Telemetry& telemetry) { return ToString(telemetry.mTyreCompound[TYRE_FRONT_LEFT]); } },
    { "TyreCompoundRR",              [](const Telemetry& telemetry) { return ToString(telemetry.mTyreCompound[TYRE_REAR_RIGHT]); } },
    { "TyreCompoundRL",              [](const Telemetry& telemetry) { return ToString(telemetry.mTyreCompound[TYRE_REAR_LEFT]); } },

    { "SnowDensity",                 [](const Telemetry& telemetry) { return ToString(telemetry.mSnowDensity); } }
};

namespace
{
    vector<string> GetTokens(const Telemetry& telemetry)
    {
        vector<string> tokens;

        for (auto column : COLUMNS)
        {
            tokens.push_back(column.second(telemetry));
        }

        return tokens;
    }

    class DataCaptureRateCalculator
    {
    public:
        DataCaptureRateCalculator(int windowSize)
            : m_circularList(windowSize)
            , m_listCursor{0}
            , m_windowSize{windowSize}
            , m_numDataAdded{ 0 }
        {
        }

        void Push(int sequenceNumber)
        {
            m_circularList[m_listCursor] = sequenceNumber;
            m_listCursor = NextIndex(m_listCursor);
            m_numDataAdded++;
        }

        double GetDataCaptureRate() const
        {
            if (!HasEnoughData())
                return -1;

            int numRecordsAvailableInWindow = m_circularList[PrevIndex(m_listCursor)] - m_circularList[m_listCursor] + 1;
            int numRecordsCapturedInWindow = m_windowSize;

            return (double)numRecordsCapturedInWindow / numRecordsAvailableInWindow;
        }

        bool HasEnoughData() const
        {
            return m_numDataAdded == m_windowSize;
        }

    private:
        int NextIndex(int index) const
        {
            return index == m_windowSize - 1 ? 0 : index + 1;
        }

        int PrevIndex(int index) const
        {
            return index == 0 ? m_windowSize - 1 : index - 1;
        }

        vector<int> m_circularList;
        int m_listCursor;
        int m_windowSize;
        int m_numDataAdded;
    };
}

int main()
{
    // Currently unused
    vector<string> sessionConstantHeaders{
        "Version",
        "BuildVersionNumber",

        "NumParticipants", //?

        "ParticipantIsActive", //?
        "ParticipantName",

        "CarName",
        "CarClassName",
        "LapsInEvent",
        "TrackLocation",
        "TrackVariation",
        "TrackLength",
    };

    vector<string> headers;

    for (auto column : COLUMNS)
    {
        headers.push_back(column.first);
    }

    TelemetryPoller poller;
    int lastIndex = 0;

    int dataCaptureRateWindowSize = 100;
    DataCaptureRateCalculator rateCalc{ dataCaptureRateWindowSize };

    UUID sessionId;
    unique_ptr<CSVFile> file{ nullptr };

    cout << "ESC to exit" << endl;
    while (!(_kbhit() && _getch() == 27))
    {
        unique_ptr<Telemetry> telemetry = poller.Next();

        if (telemetry == nullptr)
            continue;

        auto tokens = GetTokens(*telemetry);

        {
            auto currentIndex = telemetry->mSequenceNumber / 2;
            int indexChange = currentIndex - lastIndex;

            // If the sequence number wrapped around back to low numbers, we're in a new session
            if (file == nullptr || indexChange < 0)
            {
                UuidCreate(&sessionId);
                auto sessionIdStr = ToString(sessionId);
                file = make_unique<CSVFile>("Sessions\\" + sessionIdStr + ".csv", headers);
                rateCalc = DataCaptureRateCalculator{dataCaptureRateWindowSize};

                SetConsoleCursorPosition(0, 1);
                cout << "Session: " << sessionIdStr << endl;
                cout << "Data capture rate: <Waiting for data>";
            }

            rateCalc.Push(currentIndex);

            lastIndex = currentIndex;
        }

        if (rateCalc.HasEnoughData())
        {
            SetConsoleCursorPosition(19, 2);
            cout << rateCalc.GetDataCaptureRate() << "                       " << endl;
        }

        file->WriteRow(tokens);
    }

    return 0;
}
